#
#   Copyright 2011 Olivier Kozak
#
#   This file is part of Mocki.
#
#   Mocki is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public
#   License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
#   version.
#
#   Mocki is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
#   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
#
#   You should have received a copy of the GNU Lesser General Public License along with Mocki. If not, see
#   <http://www.gnu.org/licenses/>.
#

import collections

import inflection


class PartialStubbing(object):
    def __init__(self, mock, matcher):
        from mocki import actions

        self.mock, self.matcher = mock, matcher

        self.builtin_actions = actions.__dict__

    def __getattr__(self, name):
        if name.startswith('do_'):
            for action_name, action in self.builtin_actions.items():
                if name == 'do_%s' % inflection.underscore(action_name):
                    return lambda *args, **kwargs: self.do(action(*args, **kwargs))

        raise AttributeError('%r has no attribute %r' % (self, name))

    def do(self, action):
        self.mock.stub = Stub(self.matcher, action, self.mock.stub)


class Stub(object):
    def __init__(self, matcher, action, next_stub):
        self.matcher, self.action, self.next_stub = matcher, action, next_stub

    def __call__(self, call_invocation):
        if self.matcher(call_invocation):
            return self.action(call_invocation)
        else:
            return self.next_stub(call_invocation)


class PartialVerification(object):
    def __init__(self, mock, matcher):
        from mocki import expectations

        self.mock, self.matcher = mock, matcher

        self.builtin_expectations = expectations.__dict__

    def __getattr__(self, name):
        if name.startswith('invoked_'):
            for expectation_name, expectation in self.builtin_expectations.items():
                if name == 'invoked_%s' % inflection.underscore(expectation_name):
                    return lambda *args, **kwargs: self.invoked(expectation(*args, **kwargs))

        raise AttributeError('%r has no attribute %r' % (self, name))

    def invoked(self, expectation):
        do_verification(self.mock, self.matcher, expectation)

    def invoked_in_order(self, *considered_mocks):
        do_in_order_verification(self.mock, self.matcher, considered_mocks)


def do_verification(mock, matcher, expectation):
    call_invocation_history = mock.get_call_invocation_history(matcher)

    if expectation(call_invocation_history.matching_call_invocations):
        mock.verified_indices.extend([
            index for index, call_invocation in enumerate(mock.call_invocations)

            if matcher(call_invocation)
        ])
    else:
        if len(call_invocation_history.call_invocations) == 0:
            # Not working on pytest if inlined.
            message = 'No call invoked from {mock_name}.'.format(
                mock_name=mock.name,
            )

            raise AssertionError(message)

        if len(call_invocation_history.matching_indices) == 0:
            # Not working on pytest if inlined.
            message = 'Found no matching call invoked from {mock_name} :'.format(
                mock_name=mock.name,

            ) + '\n' + str(call_invocation_history)

            raise AssertionError(message)

        if len(call_invocation_history.matching_indices) == 1:
            # Not working on pytest if inlined.
            message = 'Found one matching call invoked from {mock_name} :'.format(
                mock_name=mock.name,

            ) + '\n' + str(call_invocation_history)

            raise AssertionError(message)

        if len(call_invocation_history.matching_indices) > 1:
            # Not working on pytest if inlined.
            message = 'Found {nb_calls} matching calls invoked from {mock_name} :'.format(
                nb_calls=len(call_invocation_history.matching_indices), mock_name=mock.name,

            ) + '\n' + str(call_invocation_history)

            raise AssertionError(message)


def do_in_order_verification(mock, matcher, considered_mocks):
    call_invocation_history = mock.get_in_order_call_invocation_history(matcher, considered_mocks)

    if call_invocation_history.in_order_matching_indices:
        mock.verified_indices.append(min([
            index for index, call_invocation in enumerate(mock.call_invocations)

            if matcher(call_invocation) and index > (max(mock.verified_indices) if mock.verified_indices else 0)
        ]))
    else:
        if len(call_invocation_history.call_invocations) == 0:
            # Not working on pytest if inlined.
            message = 'No call invoked from {mock_name}.'.format(
                mock_name=mock.name,
            )

            raise AssertionError(message)

        if len(call_invocation_history.matching_indices) == 0:
            # Not working on pytest if inlined.
            message = 'Found no matching call invoked from {mock_name} :'.format(
                mock_name=mock.name,

            ) + '\n' + str(call_invocation_history)

            raise AssertionError(message)

        if len(call_invocation_history.matching_indices) == 1:
            # Not working on pytest if inlined.
            message = 'Found one matching call invoked from {mock_name}, but not in order :'.format(
                mock_name=mock.name,

            ) + '\n' + str(call_invocation_history)

            raise AssertionError(message)

        if len(call_invocation_history.matching_indices) > 1:
            # Not working on pytest if inlined.
            message = 'Found {nb_calls} matching calls invoked from {mock_name}, but not in order :'.format(
                nb_calls=len(call_invocation_history.matching_indices), mock_name=mock.name,

            ) + '\n' + str(call_invocation_history)

            raise AssertionError(message)


def do_no_more_verification(mock):
    call_invocation_history = mock.get_no_more_call_invocation_history()

    if len(call_invocation_history.non_verified_indices) == 1:
        # Not working on pytest if inlined.
        message = 'Found one call invoked from {mock_name} that was not verified :'.format(
            mock_name=mock.name,

        ) + '\n' + str(call_invocation_history)

        raise AssertionError(message)

    if len(call_invocation_history.non_verified_indices) > 1:
        # Not working on pytest if inlined.
        message = 'Found {nb_calls} calls invoked from {mock_name} that was not verified :'.format(
            nb_calls=len(call_invocation_history.non_verified_indices), mock_name=mock.name,

        ) + '\n' + str(call_invocation_history)

        raise AssertionError(message)


class CallInvocationHistory(object):
    def __init__(self, call_invocations, matching_indices):
        self.call_invocations, self.matching_indices = (
            call_invocations, matching_indices
        )

    @property
    def matching_call_invocations(self):
        return [self.call_invocations[index] for index in self.matching_indices]

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        return not self.__eq__(other)

    def __str__(self):
        def history_lines_strs():
            for index, call_invocation in enumerate(self.call_invocations):
                if index in self.matching_indices:
                    yield '[{0}] {1}'.format(index + 1, call_invocation)
                else:
                    yield ' {0}  {1}'.format(index + 1, call_invocation)

        return '\n'.join(history_lines_strs())


class InOrderCallInvocationHistory(object):
    def __init__(self, call_invocations, matching_indices, verified_indices):
        self.call_invocations, self.matching_indices, self.verified_indices = (
            call_invocations, matching_indices, verified_indices
        )

    @property
    def matching_call_invocations(self):
        return [self.call_invocations[index] for index in self.matching_indices]

    @property
    def verified_call_invocations(self):
        return [self.call_invocations[index] for index in self.verified_indices]

    @property
    def in_order_matching_indices(self):
        if self.verified_indices:
            return [index for index in self.matching_indices if index > max(self.verified_indices)]
        else:
            return self.matching_indices

    @property
    def in_order_matching_call_invocations(self):
        return [self.call_invocations[index] for index in self.in_order_matching_indices]

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        return not self.__eq__(other)

    def __str__(self):
        def history_lines_strs():
            for index, call_invocation in enumerate(self.call_invocations):
                if index in self.matching_indices:
                    if index in self.verified_indices:
                        yield 'X [{0}] {1}'.format(index + 1, call_invocation)
                    else:
                        yield '  [{0}] {1}'.format(index + 1, call_invocation)
                else:
                    if index in self.verified_indices:
                        yield 'X  {0}  {1}'.format(index + 1, call_invocation)
                    else:
                        yield '   {0}  {1}'.format(index + 1, call_invocation)

        return '\n'.join(history_lines_strs())


class NoMoreCallInvocationHistory(object):
    def __init__(self, call_invocations, verified_indices):
        self.call_invocations, self.verified_indices = (
            call_invocations, verified_indices
        )

    @property
    def verified_call_invocations(self):
        return [self.call_invocations[index] for index in self.verified_indices]

    @property
    def non_verified_indices(self):
        return [index for index in range(len(self.call_invocations)) if index not in self.verified_indices]

    @property
    def non_verified_call_invocations(self):
        return [self.call_invocations[index] for index in self.non_verified_indices]

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        return not self.__eq__(other)

    def __str__(self):
        history_lines_strs = []

        for index, call_invocation in enumerate(self.call_invocations):
            if index in self.verified_indices:
                history_lines_strs.append('X {0}'.format(call_invocation))
            else:
                history_lines_strs.append('  {0}'.format(call_invocation))

        return '\n'.join(history_lines_strs)


class CallInvocation(collections.namedtuple('CallInvocation', 'target args kwargs')):
    def __str__(self):
        args, kwargs = self.args, sorted(self.kwargs.items())

        if args and kwargs:
            return '{target}({args}, {kwargs})'.format(
                target=self.target,

                args=', '.join(['{0!r}'.format(arg) for arg in args]),
                kwargs=', '.join(['{0}={1!r}'.format(*kwarg) for kwarg in kwargs]),
            )
        elif args:
            return '{target}({args})'.format(
                target=self.target,

                args=', '.join(['{0!r}'.format(arg) for arg in args]),
            )
        elif kwargs:
            return '{target}({kwargs})'.format(
                target=self.target,

                kwargs=', '.join(['{0}={1!r}'.format(*kwarg) for kwarg in kwargs]),
            )
        else:
            return '{target}()'.format(
                target=self.target,
            )
