#
#   Copyright 2011 Olivier Kozak
#
#   This file is part of Mocki.
#
#   Mocki is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public
#   License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
#   version.
#
#   Mocki is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
#   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
#
#   You should have received a copy of the GNU Lesser General Public License along with Mocki. If not, see
#   <http://www.gnu.org/licenses/>.
#

import collections

import pytest

from hamcrest import *

import mocki
import mocki.core


def test_get_mock_member_from_mock():
    mock = mocki.MockObject('theMock')

    mock_member = mock.theMember

    assert_that(mock_member, is_(instance_of(mocki.MockObject)))

    assert_that(mock_member.name, is_(equal_to('theMock.theMember')))

    assert_that(mock_member.call_invocations, is_(same_instance(mock.call_invocations)))
    assert_that(mock_member.verified_indices, is_(same_instance(mock.verified_indices)))

    assert_that(mock.members, is_(equal_to({'theMember': mock_member})))

    assert_that(mock_member, is_(same_instance(mock.theMember)))
    assert_that(mock_member, is_not(same_instance(mock.theOtherMember)))


@pytest.mark.parametrize('args, kwargs', [
    ((), {}),
    (('value1', 'value2'), {}),
    ((), {'kw1': 'value1', 'kw2': 'value2'}),
    (('value1', 'value2'), {'kw1': 'value1', 'kw2': 'value2'}),
])
def test_invoke_call_from_mock_with_no_stub_installed(args, kwargs):
    mock = mocki.MockObject('theMock')

    assert_that(mock(*args, **kwargs), is_(None))

    assert_that(mock.call_invocations, is_(equal_to([mocki.core.CallInvocation(mock, args, kwargs)])))


@pytest.mark.parametrize('args, kwargs', [
    ((), {}),
    (('value1', 'value2'), {}),
    ((), {'kw1': 'value1', 'kw2': 'value2'}),
    (('value1', 'value2'), {'kw1': 'value1', 'kw2': 'value2'}),
])
def test_invoke_call_from_mock_with_stub_installed(args, kwargs):
    mock = mocki.MockObject('theMock')

    mock.stub = lambda call_invocation: call_invocation

    assert_that(mock(*args, **kwargs), is_(equal_to(mocki.core.CallInvocation(mock, args, kwargs))))

    assert_that(mock.call_invocations, is_(equal_to([mocki.core.CallInvocation(mock, args, kwargs)])))


def test_get_all_members_of_mock():
    mock = mocki.MockObject('theMock')

    member_of_mock = mocki.MockObject('theMember')
    other_member_of_mock = mocki.MockObject('theOtherMember')

    mock.members = {
        'theMember': member_of_mock,
        'theOtherMember': other_member_of_mock,
    }

    member_of_member_of_mock = mocki.MockObject('theMember')
    other_member_of_member_of_mock = mocki.MockObject('theOtherMember')

    member_of_mock.members = {
        'theMember': member_of_member_of_mock,
        'theOtherMember': other_member_of_member_of_mock,
    }

    member_of_member_of_member_of_mock = mocki.MockObject('theMember')
    other_member_of_member_of_member_of_mock = mocki.MockObject('theOtherMember')

    member_of_member_of_mock.members = {
        'theMember': member_of_member_of_member_of_mock,
        'theOtherMember': other_member_of_member_of_member_of_mock,
    }

    assert_that(mock.all_members, contains_inanyorder(
        member_of_mock,
        other_member_of_mock,
        member_of_member_of_mock,
        other_member_of_member_of_mock,
        member_of_member_of_member_of_mock,
        other_member_of_member_of_member_of_mock,
    ))


def test_get_call_invocation_history_from_mock():
    # noinspection PyPep8Naming
    CallInvocation = collections.namedtuple('CallInvocation', 'target name')

    mock, other_mock = mocki.MockObject('theMock'), mocki.MockObject('theOtherMock')

    mock.call_invocations = [
        CallInvocation(mock, '1stCall'),
        CallInvocation(other_mock, '2ndCall'),
        CallInvocation(mock, '3rdCall'),
        CallInvocation(other_mock, '4thCall'),
        CallInvocation(mock, '5thCall'),
        CallInvocation(other_mock, '6thCall'),
        CallInvocation(mock, '7thCall'),
        CallInvocation(other_mock, '8thCall'),
    ]

    def matcher(call_invocation):
        return call_invocation.name in ['3rdCall', '4thCall', '7thCall', '8thCall']

    assert_that(mock.get_call_invocation_history(matcher), is_(equal_to(
        mocki.core.CallInvocationHistory([
            CallInvocation(mock, '1stCall'),
            CallInvocation(mock, '3rdCall'),
            CallInvocation(mock, '5thCall'),
            CallInvocation(mock, '7thCall'),

        ], matching_indices=[1, 3])
    )))


def test_get_in_order_call_invocation_history_from_mock():
    # noinspection PyPep8Naming
    CallInvocation = collections.namedtuple('CallInvocation', 'target name')

    mock, other_mock = mocki.MockObject('theMock'), mocki.MockObject('theOtherMock')

    member_of_mock = mocki.MockObject('theMember')
    other_member_of_mock = mocki.MockObject('theOtherMember')

    mock.members = {
        'theMember': member_of_mock,
        'theOtherMember': other_member_of_mock,
    }

    member_of_member_of_mock = mocki.MockObject('theMember')

    member_of_mock.members = {
        'theMember': member_of_member_of_mock,
    }

    member_of_other_member_of_mock = mocki.MockObject('theOtherMember')

    other_member_of_mock.members = {
        'theOtherMember': member_of_other_member_of_mock,
    }

    mock.call_invocations = [
        CallInvocation(mock, '1stCall'),
        CallInvocation(other_mock, '2ndCall'),
        CallInvocation(mock, '3rdCall'),
        CallInvocation(other_mock, '4thCall'),
        CallInvocation(member_of_member_of_mock, '5thCall'),
        CallInvocation(other_mock, '6thCall'),
        CallInvocation(member_of_other_member_of_mock, '7thCall'),
        CallInvocation(other_mock, '8thCall'),
    ]

    mock.verified_indices = [4, 5, 6, 7]

    def matcher(call_invocation):
        return call_invocation.name in ['3rdCall', '4thCall', '7thCall', '8thCall']

    considered_mocks = [member_of_mock, other_member_of_mock]

    assert_that(mock.get_in_order_call_invocation_history(matcher, considered_mocks), is_(equal_to(
        mocki.core.InOrderCallInvocationHistory([
            CallInvocation(mock, '3rdCall'),
            CallInvocation(member_of_member_of_mock, '5thCall'),
            CallInvocation(member_of_other_member_of_mock, '7thCall'),

        ], matching_indices=[0, 2], verified_indices=[1, 2])
    )))


def test_get_no_more_call_invocation_history_from_mock():
    # noinspection PyPep8Naming
    CallInvocation = collections.namedtuple('CallInvocation', 'target name')

    mock, other_mock = mocki.MockObject('theMock'), mocki.MockObject('theOtherMock')

    member_of_mock = mocki.MockObject('theMember')
    other_member_of_mock = mocki.MockObject('theOtherMember')

    mock.members = {
        'theMember': member_of_mock,
        'theOtherMember': other_member_of_mock,
    }

    mock.call_invocations = [
        CallInvocation(mock, '1stCall'),
        CallInvocation(other_mock, '2ndCall'),
        CallInvocation(member_of_mock, '3rdCall'),
        CallInvocation(other_mock, '4thCall'),
        CallInvocation(mock, '5thCall'),
        CallInvocation(other_mock, '6thCall'),
        CallInvocation(other_member_of_mock, '7thCall'),
        CallInvocation(other_mock, '8thCall'),
    ]

    mock.verified_indices = [4, 5, 6, 7]

    assert_that(mock.get_no_more_call_invocation_history(), is_(equal_to(
        mocki.core.NoMoreCallInvocationHistory([
            CallInvocation(mock, '1stCall'),
            CallInvocation(member_of_mock, '3rdCall'),
            CallInvocation(mock, '5thCall'),
            CallInvocation(other_member_of_mock, '7thCall'),

        ], verified_indices=[2, 3])
    )))


def test_start_stubbing_from_mock():
    mock = mocki.MockObject('theMock')

    partial_stubbing = mock.on('theMatcher')

    assert_that(partial_stubbing, is_(instance_of(mocki.core.PartialStubbing)))

    assert_that(partial_stubbing.mock, is_(same_instance(mock)))

    assert_that(partial_stubbing.matcher, is_(equal_to('theMatcher')))


@pytest.mark.parametrize('args, kwargs', [
    ((), {}),
    (('value1', 'value2'), {}),
    ((), {'kw1': 'value1', 'kw2': 'value2'}),
    (('value1', 'value2'), {'kw1': 'value1', 'kw2': 'value2'}),
])
def test_start_stubbing_from_mock_using_builtin_matcher(args, kwargs):
    class BuiltinMatcherFakeType(object):
        def __init__(self, matcher):
            self.matcher = matcher

        def __call__(self, *this_args, **this_kwargs):
            return self.matcher if (this_args, this_kwargs) == (args, kwargs) else None

    mock = mocki.MockObject('theMock')

    mock.builtin_matchers = {
        'BuiltinMatcher': BuiltinMatcherFakeType('theMatcher'),
        'OtherBuiltinMatcher': BuiltinMatcherFakeType('theOtherMatcher'),
    }

    partial_stubbing = mock.on_builtin_matcher(*args, **kwargs)

    assert_that(partial_stubbing, is_(instance_of(mocki.core.PartialStubbing)))

    assert_that(partial_stubbing.mock, is_(same_instance(mock)))

    assert_that(partial_stubbing.matcher, is_(equal_to('theMatcher')))


def test_start_verification_from_mock():
    mock = mocki.MockObject('theMock')

    partial_verification = mock.verify('theMatcher')

    assert_that(partial_verification, is_(instance_of(mocki.core.PartialVerification)))

    assert_that(partial_verification.mock, is_(same_instance(mock)))

    assert_that(partial_verification.matcher, is_(equal_to('theMatcher')))


@pytest.mark.parametrize('args, kwargs', [
    ((), {}),
    (('value1', 'value2'), {}),
    ((), {'kw1': 'value1', 'kw2': 'value2'}),
    (('value1', 'value2'), {'kw1': 'value1', 'kw2': 'value2'}),
])
def test_start_verification_from_mock_using_builtin_matcher(args, kwargs):
    class BuiltinMatcherFakeType(object):
        def __init__(self, matcher):
            self.matcher = matcher

        def __call__(self, *this_args, **this_kwargs):
            return self.matcher if (this_args, this_kwargs) == (args, kwargs) else None

    mock = mocki.MockObject('theMock')

    mock.builtin_matchers = {
        'BuiltinMatcher': BuiltinMatcherFakeType('theMatcher'),
        'OtherBuiltinMatcher': BuiltinMatcherFakeType('theOtherMatcher'),
    }

    partial_verification = mock.verify_builtin_matcher(*args, **kwargs)

    assert_that(partial_verification, is_(instance_of(mocki.core.PartialVerification)))

    assert_that(partial_verification.mock, is_(same_instance(mock)))

    assert_that(partial_verification.matcher, is_(equal_to('theMatcher')))


def test_finish_stubbing_from_partial_stubbing():
    mock = mocki.MockObject('theMock')

    mock.stub = 'theInitialStub'

    partial_stubbing = mocki.core.PartialStubbing(mock, 'theMatcher')

    partial_stubbing.do('theAction')

    assert_that(mock.stub, is_(instance_of(mocki.core.Stub)))

    # noinspection PyUnresolvedReferences
    assert_that(mock.stub.matcher, is_(equal_to('theMatcher')))
    # noinspection PyUnresolvedReferences
    assert_that(mock.stub.action, is_(equal_to('theAction')))
    # noinspection PyUnresolvedReferences
    assert_that(mock.stub.next_stub, is_(equal_to('theInitialStub')))


@pytest.mark.parametrize('args, kwargs', [
    ((), {}),
    (('value1', 'value2'), {}),
    ((), {'kw1': 'value1', 'kw2': 'value2'}),
    (('value1', 'value2'), {'kw1': 'value1', 'kw2': 'value2'}),
])
def test_finish_stubbing_from_partial_stubbing_using_builtin_action(args, kwargs):
    class BuiltinActionFakeType(object):
        def __init__(self, action):
            self.action = action

        def __call__(self, *this_args, **this_kwargs):
            return self.action if (this_args, this_kwargs) == (args, kwargs) else None

    mock = mocki.MockObject('theMock')

    mock.stub = 'theInitialStub'

    partial_stubbing = mocki.core.PartialStubbing(mock, 'theMatcher')

    partial_stubbing.builtin_actions = {
        'BuiltinAction': BuiltinActionFakeType('theAction'),
        'OtherBuiltinAction': BuiltinActionFakeType('theOtherAction'),
    }

    partial_stubbing.do_builtin_action(*args, **kwargs)

    assert_that(mock.stub, is_(instance_of(mocki.core.Stub)))

    # noinspection PyUnresolvedReferences
    assert_that(mock.stub.matcher, is_(equal_to('theMatcher')))
    # noinspection PyUnresolvedReferences
    assert_that(mock.stub.action, is_(equal_to('theAction')))
    # noinspection PyUnresolvedReferences
    assert_that(mock.stub.next_stub, is_(equal_to('theInitialStub')))


def test_apply_stub_to_matching_call_invocation():
    # noinspection PyPep8Naming
    CallInvocation = collections.namedtuple('CallInvocation', 'name')

    stub = mocki.core.Stub(
        matcher=lambda call_invocation: call_invocation == CallInvocation(name='1stCall'),
        action=lambda call_invocation: call_invocation,

        next_stub=None,
    )

    assert_that(stub(CallInvocation(name='1stCall')), is_(equal_to(CallInvocation(name='1stCall'))))


def test_apply_stub_to_non_matching_call_invocation():
    # noinspection PyPep8Naming
    CallInvocation = collections.namedtuple('CallInvocation', 'name')

    stub = mocki.core.Stub(
        matcher=lambda call_invocation: call_invocation != CallInvocation(name='1stCall'),
        next_stub=lambda call_invocation: call_invocation,

        action=None,
    )

    assert_that(stub(CallInvocation(name='1stCall')), is_(equal_to(CallInvocation(name='1stCall'))))


def test_finish_verification_from_partial_verification(monkeypatch):
    class DoVerificationFake(object):
        def __call__(self, mock, matcher, expectation):
            # noinspection PyAttributeOutsideInit
            self.mock, self.matcher, self.expectation = mock, matcher, expectation

    do_verification = DoVerificationFake()

    monkeypatch.setattr('mocki.core.do_verification', do_verification)

    partial_verification = mocki.core.PartialVerification('theMock', 'theMatcher')

    partial_verification.invoked('theExpectation')

    assert_that(do_verification.mock, is_(equal_to('theMock')))
    assert_that(do_verification.matcher, is_(equal_to('theMatcher')))
    assert_that(do_verification.expectation, is_(equal_to('theExpectation')))


@pytest.mark.parametrize('args, kwargs', [
    ((), {}),
    (('value1', 'value2'), {}),
    ((), {'kw1': 'value1', 'kw2': 'value2'}),
    (('value1', 'value2'), {'kw1': 'value1', 'kw2': 'value2'}),
])
def test_finish_verification_from_partial_verification_using_builtin_expectation(args, kwargs, monkeypatch):
    class DoVerificationFake(object):
        def __call__(self, mock, matcher, expectation):
            # noinspection PyAttributeOutsideInit
            self.mock, self.matcher, self.expectation = mock, matcher, expectation

    class BuiltinExpectationFakeType(object):
        def __init__(self, expectation):
            self.expectation = expectation

        def __call__(self, *this_args, **this_kwargs):
            return self.expectation if (this_args, this_kwargs) == (args, kwargs) else None

    do_verification = DoVerificationFake()

    monkeypatch.setattr('mocki.core.do_verification', do_verification)

    partial_verification = mocki.core.PartialVerification('theMock', 'theMatcher')

    partial_verification.builtin_expectations = {
        'BuiltinExpectation': BuiltinExpectationFakeType('theExpectation'),
        'OtherBuiltinExpectation': BuiltinExpectationFakeType('theOtherExpectation'),
    }

    partial_verification.invoked_builtin_expectation(*args, **kwargs)

    assert_that(do_verification.mock, is_(equal_to('theMock')))
    assert_that(do_verification.matcher, is_(equal_to('theMatcher')))
    assert_that(do_verification.expectation, is_(equal_to('theExpectation')))


def test_finish_in_order_verification_from_partial_verification(monkeypatch):
    class DoInOrderVerificationFake(object):
        def __call__(self, mock, matcher, considered_mocks):
            # noinspection PyAttributeOutsideInit
            self.mock, self.matcher, self.considered_mocks = mock, matcher, considered_mocks

    do_in_order_verification = DoInOrderVerificationFake()

    monkeypatch.setattr('mocki.core.do_in_order_verification', do_in_order_verification)

    partial_verification = mocki.core.PartialVerification('theMock', 'theMatcher')

    partial_verification.invoked_in_order('theConsideredMock', 'theOtherConsideredMock')

    assert_that(do_in_order_verification.mock, is_(equal_to('theMock')))
    assert_that(do_in_order_verification.matcher, is_(equal_to('theMatcher')))
    assert_that(do_in_order_verification.considered_mocks, contains('theConsideredMock', 'theOtherConsideredMock'))


def test_do_verification():
    # noinspection PyPep8Naming
    CallInvocation = collections.namedtuple('CallInvocation', 'target name')

    mock = mocki.MockObject('theMock')

    mock.call_invocations = [
        CallInvocation(mock, name='1stCall'),
        CallInvocation(mock, name='2ndCall'),
        CallInvocation(mock, name='3rdCall'),
        CallInvocation(mock, name='4thCall'),
    ]

    def matcher(call_invocation):
        return call_invocation in [
            CallInvocation(mock, name='2ndCall'),
            CallInvocation(mock, name='4thCall'),
        ]

    def expectation(call_invocations):
        return call_invocations == [
            CallInvocation(mock, name='2ndCall'),
            CallInvocation(mock, name='4thCall'),
        ]

    mocki.core.do_verification(mock, matcher, expectation)

    assert_that(mock.verified_indices, contains(1, 3))


def test_do_verification_failing_with_no_call_invoked():
    mock = mocki.MockObject('theMock')

    mock.call_invocations = []

    def matcher(call_invocation):
        return call_invocation in []

    def expectation(call_invocations):
        return call_invocations != []

    with pytest.raises(AssertionError) as excinfo:
        mocki.core.do_verification(mock, matcher, expectation)

    assert_that(str(excinfo.value), is_(equal_to('No call invoked from theMock.')))

    assert_that(mock.verified_indices, contains())


def test_do_verification_failing_with_no_matching_call_invoked():
    class CallInvocation(collections.namedtuple('CallInvocation', 'target name')):
        def __str__(self):
            return self.name

    mock = mocki.MockObject('theMock')

    mock.call_invocations = [
        CallInvocation(mock, name='1stCall'),
        CallInvocation(mock, name='2ndCall'),
        CallInvocation(mock, name='3rdCall'),
        CallInvocation(mock, name='4thCall'),
    ]

    def matcher(call_invocation):
        return call_invocation in []

    def expectation(call_invocations):
        return call_invocations != []

    with pytest.raises(AssertionError) as excinfo:
        mocki.core.do_verification(mock, matcher, expectation)

    assert_that(str(excinfo.value), is_(equal_to('\n'.join([
        'Found no matching call invoked from theMock :',

        ' 1  1stCall',
        ' 2  2ndCall',
        ' 3  3rdCall',
        ' 4  4thCall',
    ]))))

    assert_that(mock.verified_indices, contains())


def test_do_verification_failing_with_one_matching_call_invoked():
    class CallInvocation(collections.namedtuple('CallInvocation', 'target name')):
        def __str__(self):
            return self.name

    mock = mocki.MockObject('theMock')

    mock.call_invocations = [
        CallInvocation(mock, name='1stCall'),
        CallInvocation(mock, name='2ndCall'),
        CallInvocation(mock, name='3rdCall'),
        CallInvocation(mock, name='4thCall'),
    ]

    def matcher(call_invocation):
        return call_invocation in [
            CallInvocation(mock, name='2ndCall'),
        ]

    def expectation(call_invocations):
        return call_invocations != [
            CallInvocation(mock, name='2ndCall'),
        ]

    with pytest.raises(AssertionError) as excinfo:
        mocki.core.do_verification(mock, matcher, expectation)

    assert_that(str(excinfo.value), is_(equal_to('\n'.join([
        'Found one matching call invoked from theMock :',

        ' 1  1stCall',
        '[2] 2ndCall',
        ' 3  3rdCall',
        ' 4  4thCall',
    ]))))

    assert_that(mock.verified_indices, contains())


def test_do_verification_failing_with_several_matching_calls_invoked():
    class CallInvocation(collections.namedtuple('CallInvocation', 'target name')):
        def __str__(self):
            return self.name

    mock = mocki.MockObject('theMock')

    mock.call_invocations = [
        CallInvocation(mock, name='1stCall'),
        CallInvocation(mock, name='2ndCall'),
        CallInvocation(mock, name='3rdCall'),
        CallInvocation(mock, name='4thCall'),
    ]

    def matcher(call_invocation):
        return call_invocation in [
            CallInvocation(mock, name='2ndCall'),
            CallInvocation(mock, name='4thCall'),
        ]

    def expectation(call_invocations):
        return call_invocations != [
            CallInvocation(mock, name='2ndCall'),
            CallInvocation(mock, name='4thCall'),
        ]

    with pytest.raises(AssertionError) as excinfo:
        mocki.core.do_verification(mock, matcher, expectation)

    assert_that(str(excinfo.value), is_(equal_to('\n'.join([
        'Found 2 matching calls invoked from theMock :',

        ' 1  1stCall',
        '[2] 2ndCall',
        ' 3  3rdCall',
        '[4] 4thCall',
    ]))))

    assert_that(mock.verified_indices, contains())


def test_do_in_order_verification_with_no_verified_index():
    # noinspection PyPep8Naming
    CallInvocation = collections.namedtuple('CallInvocation', 'target name')

    mock = mocki.MockObject('theMock')

    mock.call_invocations = [
        CallInvocation(mock, name='1stCall'),
        CallInvocation(mock, name='2ndCall'),
        CallInvocation(mock, name='3rdCall'),
        CallInvocation(mock, name='4thCall'),
        CallInvocation(mock, name='5thCall'),
        CallInvocation(mock, name='6thCall'),
    ]

    mock.verified_indices = []

    def matcher(call_invocation):
        return call_invocation in [
            CallInvocation(mock, name='2ndCall'),
            CallInvocation(mock, name='4thCall'),
            CallInvocation(mock, name='6thCall'),
        ]

    mocki.core.do_in_order_verification(mock, matcher, considered_mocks=[mock])

    assert_that(mock.verified_indices, contains(1))


def test_do_in_order_verification_with_verified_indices():
    # noinspection PyPep8Naming
    CallInvocation = collections.namedtuple('CallInvocation', 'target name')

    mock = mocki.MockObject('theMock')

    mock.call_invocations = [
        CallInvocation(mock, name='1stCall'),
        CallInvocation(mock, name='2ndCall'),
        CallInvocation(mock, name='3rdCall'),
        CallInvocation(mock, name='4thCall'),
        CallInvocation(mock, name='5thCall'),
        CallInvocation(mock, name='6thCall'),
    ]

    mock.verified_indices = [0, 1]

    def matcher(call_invocation):
        return call_invocation in [
            CallInvocation(mock, name='2ndCall'),
            CallInvocation(mock, name='4thCall'),
            CallInvocation(mock, name='6thCall'),
        ]

    mocki.core.do_in_order_verification(mock, matcher, considered_mocks=[mock])

    assert_that(mock.verified_indices, contains(0, 1, 3))


def test_do_in_order_verification_failing_with_no_call_invoked():
    mock = mocki.MockObject('theMock')

    mock.call_invocations = []

    mock.verified_indices = []

    def matcher(call_invocation):
        return call_invocation in []

    with pytest.raises(AssertionError) as excinfo:
        mocki.core.do_in_order_verification(mock, matcher, considered_mocks=[mock])

    assert_that(str(excinfo.value), is_(equal_to('No call invoked from theMock.')))

    assert_that(mock.verified_indices, contains())


def test_do_in_order_verification_failing_with_no_matching_call_invoked():
    class CallInvocation(collections.namedtuple('CallInvocation', 'target name')):
        def __str__(self):
            return self.name

    mock = mocki.MockObject('theMock')

    mock.call_invocations = [
        CallInvocation(mock, name='1stCall'),
        CallInvocation(mock, name='2ndCall'),
        CallInvocation(mock, name='3rdCall'),
        CallInvocation(mock, name='4thCall'),
        CallInvocation(mock, name='5thCall'),
        CallInvocation(mock, name='6thCall'),
    ]

    mock.verified_indices = [4, 5]

    def matcher(call_invocation):
        return call_invocation in []

    with pytest.raises(AssertionError) as excinfo:
        mocki.core.do_in_order_verification(mock, matcher, considered_mocks=[mock])

    assert_that(str(excinfo.value), is_(equal_to('\n'.join([
        'Found no matching call invoked from theMock :',

        '   1  1stCall',
        '   2  2ndCall',
        '   3  3rdCall',
        '   4  4thCall',
        'X  5  5thCall',
        'X  6  6thCall',
    ]))))

    assert_that(mock.verified_indices, contains(4, 5))


def test_do_in_order_verification_failing_with_one_matching_call_invoked_but_not_in_order():
    class CallInvocation(collections.namedtuple('CallInvocation', 'target name')):
        def __str__(self):
            return self.name

    mock = mocki.MockObject('theMock')

    mock.call_invocations = [
        CallInvocation(mock, name='1stCall'),
        CallInvocation(mock, name='2ndCall'),
        CallInvocation(mock, name='3rdCall'),
        CallInvocation(mock, name='4thCall'),
        CallInvocation(mock, name='5thCall'),
        CallInvocation(mock, name='6thCall'),
    ]

    mock.verified_indices = [4, 5]

    def matcher(call_invocation):
        return call_invocation in [
            CallInvocation(mock, name='2ndCall'),
        ]

    with pytest.raises(AssertionError) as excinfo:
        mocki.core.do_in_order_verification(mock, matcher, considered_mocks=[mock])

    assert_that(str(excinfo.value), is_(equal_to('\n'.join([
        'Found one matching call invoked from theMock, but not in order :',

        '   1  1stCall',
        '  [2] 2ndCall',
        '   3  3rdCall',
        '   4  4thCall',
        'X  5  5thCall',
        'X  6  6thCall',
    ]))))

    assert_that(mock.verified_indices, contains(4, 5))


def test_do_in_order_verification_failing_with_several_matching_calls_invoked_but_not_in_order():
    class CallInvocation(collections.namedtuple('CallInvocation', 'target name')):
        def __str__(self):
            return self.name

    mock = mocki.MockObject('theMock')

    mock.call_invocations = [
        CallInvocation(mock, name='1stCall'),
        CallInvocation(mock, name='2ndCall'),
        CallInvocation(mock, name='3rdCall'),
        CallInvocation(mock, name='4thCall'),
        CallInvocation(mock, name='5thCall'),
        CallInvocation(mock, name='6thCall'),
    ]

    mock.verified_indices = [4, 5]

    def matcher(call_invocation):
        return call_invocation in [
            CallInvocation(mock, name='2ndCall'),
            CallInvocation(mock, name='4thCall'),
        ]

    with pytest.raises(AssertionError) as excinfo:
        mocki.core.do_in_order_verification(mock, matcher, considered_mocks=[mock])

    assert_that(str(excinfo.value), is_(equal_to('\n'.join([
        'Found 2 matching calls invoked from theMock, but not in order :',

        '   1  1stCall',
        '  [2] 2ndCall',
        '   3  3rdCall',
        '  [4] 4thCall',
        'X  5  5thCall',
        'X  6  6thCall',
    ]))))

    assert_that(mock.verified_indices, contains(4, 5))


def test_do_no_more_verification():
    # noinspection PyPep8Naming
    CallInvocation = collections.namedtuple('CallInvocation', 'target name')

    mock = mocki.MockObject('theMock')

    mock.call_invocations = [
        CallInvocation(mock, name='1stCall'),
        CallInvocation(mock, name='2ndCall'),
        CallInvocation(mock, name='3rdCall'),
    ]

    mock.verified_indices = [0, 1, 2]

    mocki.core.do_no_more_verification(mock)


def test_do_no_more_verification_failing_with_one_non_verified_call_invoked():
    class CallInvocation(collections.namedtuple('CallInvocation', 'target name')):
        def __str__(self):
            return self.name

    mock = mocki.MockObject('theMock')

    mock.call_invocations = [
        CallInvocation(mock, name='1stCall'),
        CallInvocation(mock, name='2ndCall'),
        CallInvocation(mock, name='3rdCall'),
    ]

    mock.verified_indices = [0, 2]

    with pytest.raises(AssertionError) as excinfo:
        mocki.core.do_no_more_verification(mock)

    assert_that(str(excinfo.value), is_(equal_to('\n'.join([
        'Found one call invoked from theMock that was not verified :',

        'X 1stCall',
        '  2ndCall',
        'X 3rdCall',
    ]))))


def test_do_no_more_verification_failing_with_several_non_verified_call_invoked():
    class CallInvocation(collections.namedtuple('CallInvocation', 'target name')):
        def __str__(self):
            return self.name

    mock = mocki.MockObject('theMock')

    mock.call_invocations = [
        CallInvocation(mock, name='1stCall'),
        CallInvocation(mock, name='2ndCall'),
        CallInvocation(mock, name='3rdCall'),
    ]

    mock.verified_indices = []

    with pytest.raises(AssertionError) as excinfo:
        mocki.core.do_no_more_verification(mock)

    assert_that(str(excinfo.value), is_(equal_to('\n'.join([
        'Found 3 calls invoked from theMock that was not verified :',

        '  1stCall',
        '  2ndCall',
        '  3rdCall',
    ]))))


# noinspection PyMethodMayBeStatic
class TestCallInvocationHistory(object):
    class TestWithNoMatchingIndex(object):
        def setup_method(self, _):
            # noinspection PyAttributeOutsideInit
            self.call_invocation_history = mocki.core.CallInvocationHistory(
                call_invocations=[
                    '1stCall',
                    '2ndCall',
                    '3rdCall',
                    '4thCall',
                ],

                matching_indices=[]
            )

        def test_get_call_invocations(self):
            assert_that(self.call_invocation_history.call_invocations, contains(
                '1stCall',
                '2ndCall',
                '3rdCall',
                '4thCall',
            ))

        def test_get_matching_indices(self):
            assert_that(self.call_invocation_history.matching_indices, contains())

        def test_get_matching_call_invocations(self):
            assert_that(self.call_invocation_history.matching_call_invocations, contains())

    class TestWithMatchingIndices(object):
        def setup_method(self, _):
            # noinspection PyAttributeOutsideInit
            self.call_invocation_history = mocki.core.CallInvocationHistory(
                call_invocations=[
                    '1stCall',
                    '2ndCall',
                    '3rdCall',
                    '4thCall',
                ],

                matching_indices=[1, 3]
            )

        def test_get_call_invocations(self):
            assert_that(self.call_invocation_history.call_invocations, contains(
                '1stCall',
                '2ndCall',
                '3rdCall',
                '4thCall',
            ))

        def test_get_matching_indices(self):
            assert_that(self.call_invocation_history.matching_indices, contains(1, 3))

        def test_get_matching_call_invocations(self):
            assert_that(self.call_invocation_history.matching_call_invocations, contains(
                '2ndCall',
                '4thCall',
            ))

    def test_str(self):
        call_invocation_history = mocki.core.CallInvocationHistory(
            call_invocations=[
                '1stCall',
                '2ndCall',
                '3rdCall',
                '4thCall',
            ],

            matching_indices=[1, 3]
        )

        assert_that(str(call_invocation_history), is_(equal_to('\n'.join([
            ' 1  1stCall',
            '[2] 2ndCall',
            ' 3  3rdCall',
            '[4] 4thCall',
        ]))))


# noinspection PyMethodMayBeStatic
class TestInOrderCallInvocationHistory(object):
    class TestWithNoMatchingIndexOrVerifiedIndex(object):
        def setup_method(self, _):
            # noinspection PyAttributeOutsideInit
            self.call_invocation_history = mocki.core.InOrderCallInvocationHistory(
                call_invocations=[
                    '1stCall',
                    '2ndCall',
                    '3rdCall',
                    '4thCall',
                ],

                matching_indices=[], verified_indices=[]
            )

        def test_get_call_invocations(self):
            assert_that(self.call_invocation_history.call_invocations, contains(
                '1stCall',
                '2ndCall',
                '3rdCall',
                '4thCall',
            ))

        def test_get_matching_indices(self):
            assert_that(self.call_invocation_history.matching_indices, contains())

        def test_get_matching_call_invocations(self):
            assert_that(self.call_invocation_history.matching_call_invocations, contains())

        def test_get_verified_indices(self):
            assert_that(self.call_invocation_history.verified_indices, contains())

        def test_get_verified_call_invocations(self):
            assert_that(self.call_invocation_history.verified_call_invocations, contains())

        def test_get_in_order_matching_indices(self):
            assert_that(self.call_invocation_history.in_order_matching_indices, contains())

        def test_get_in_order_matching_call_invocations(self):
            assert_that(self.call_invocation_history.in_order_matching_call_invocations, contains())

    class TestWithMatchingIndicesButNoVerifiedIndex(object):
        def setup_method(self, _):
            # noinspection PyAttributeOutsideInit
            self.call_invocation_history = mocki.core.InOrderCallInvocationHistory(
                call_invocations=[
                    '1stCall',
                    '2ndCall',
                    '3rdCall',
                    '4thCall',
                    '5thCall',
                    '6thCall',
                    '7thCall',
                    '8thCall',
                ],

                matching_indices=[1, 3, 5, 7], verified_indices=[]
            )

        def test_get_call_invocations(self):
            assert_that(self.call_invocation_history.call_invocations, contains(
                '1stCall',
                '2ndCall',
                '3rdCall',
                '4thCall',
                '5thCall',
                '6thCall',
                '7thCall',
                '8thCall',
            ))

        def test_get_matching_indices(self):
            assert_that(self.call_invocation_history.matching_indices, contains(1, 3, 5, 7))

        def test_get_matching_call_invocations(self):
            assert_that(self.call_invocation_history.matching_call_invocations, contains(
                '2ndCall',
                '4thCall',
                '6thCall',
                '8thCall',
            ))

        def test_get_verified_indices(self):
            assert_that(self.call_invocation_history.verified_indices, contains())

        def test_get_verified_call_invocations(self):
            assert_that(self.call_invocation_history.verified_call_invocations, contains())

        def test_get_in_order_matching_indices(self):
            assert_that(self.call_invocation_history.in_order_matching_indices, contains(1, 3, 5, 7))

        def test_get_in_order_matching_call_invocations(self):
            assert_that(self.call_invocation_history.in_order_matching_call_invocations, contains(
                '2ndCall',
                '4thCall',
                '6thCall',
                '8thCall',
            ))

    class TestWithVerifiedIndicesButNoMatchingIndex(object):
        def setup_method(self, _):
            # noinspection PyAttributeOutsideInit
            self.call_invocation_history = mocki.core.InOrderCallInvocationHistory(
                call_invocations=[
                    '1stCall',
                    '2ndCall',
                    '3rdCall',
                    '4thCall',
                    '5thCall',
                    '6thCall',
                    '7thCall',
                    '8thCall',
                ],

                matching_indices=[], verified_indices=[2, 3]
            )

        def test_get_call_invocations(self):
            assert_that(self.call_invocation_history.call_invocations, contains(
                '1stCall',
                '2ndCall',
                '3rdCall',
                '4thCall',
                '5thCall',
                '6thCall',
                '7thCall',
                '8thCall',
            ))

        def test_get_matching_indices(self):
            assert_that(self.call_invocation_history.matching_indices, contains())

        def test_get_matching_call_invocations(self):
            assert_that(self.call_invocation_history.matching_call_invocations, contains())

        def test_get_verified_indices(self):
            assert_that(self.call_invocation_history.verified_indices, contains(2, 3))

        def test_get_verified_call_invocations(self):
            assert_that(self.call_invocation_history.verified_call_invocations, contains(
                '3rdCall',
                '4thCall',
            ))

        def test_get_in_order_matching_indices(self):
            assert_that(self.call_invocation_history.in_order_matching_indices, contains())

        def test_get_in_order_matching_call_invocations(self):
            assert_that(self.call_invocation_history.in_order_matching_call_invocations, contains())

    class TestWithMatchingIndicesAndVerifiedIndices(object):
        def setup_method(self, _):
            # noinspection PyAttributeOutsideInit
            self.call_invocation_history = mocki.core.InOrderCallInvocationHistory(
                call_invocations=[
                    '1stCall',
                    '2ndCall',
                    '3rdCall',
                    '4thCall',
                    '5thCall',
                    '6thCall',
                    '7thCall',
                    '8thCall',
                ],

                matching_indices=[1, 3, 5, 7], verified_indices=[2, 3]
            )

        def test_get_call_invocations(self):
            assert_that(self.call_invocation_history.call_invocations, contains(
                '1stCall',
                '2ndCall',
                '3rdCall',
                '4thCall',
                '5thCall',
                '6thCall',
                '7thCall',
                '8thCall',
            ))

        def test_get_matching_indices(self):
            assert_that(self.call_invocation_history.matching_indices, contains(1, 3, 5, 7))

        def test_get_matching_call_invocations(self):
            assert_that(self.call_invocation_history.matching_call_invocations, contains(
                '2ndCall',
                '4thCall',
                '6thCall',
                '8thCall',
            ))

        def test_get_verified_indices(self):
            assert_that(self.call_invocation_history.verified_indices, contains(2, 3))

        def test_get_verified_call_invocations(self):
            assert_that(self.call_invocation_history.verified_call_invocations, contains(
                '3rdCall',
                '4thCall',
            ))

        def test_get_in_order_matching_indices(self):
            assert_that(self.call_invocation_history.in_order_matching_indices, contains(5, 7))

        def test_get_in_order_matching_call_invocations(self):
            assert_that(self.call_invocation_history.in_order_matching_call_invocations, contains(
                '6thCall',
                '8thCall',
            ))

    def test_str(self):
        call_invocation_history = mocki.core.InOrderCallInvocationHistory(
            call_invocations=[
                '1stCall',
                '2ndCall',
                '3rdCall',
                '4thCall',
            ],

            matching_indices=[1, 3], verified_indices=[2, 3]
        )

        assert_that(str(call_invocation_history), is_(equal_to('\n'.join([
            '   1  1stCall',
            '  [2] 2ndCall',
            'X  3  3rdCall',
            'X [4] 4thCall',
        ]))))


# noinspection PyMethodMayBeStatic
class TestNoMoreCallInvocationHistory(object):
    class TestWithNoVerifiedIndex(object):
        def setup_method(self, _):
            # noinspection PyAttributeOutsideInit
            self.call_invocation_history = mocki.core.NoMoreCallInvocationHistory(
                call_invocations=[
                    '1stCall',
                    '2ndCall',
                    '3rdCall',
                    '4thCall',
                ],

                verified_indices=[]
            )

        def test_get_call_invocations(self):
            assert_that(self.call_invocation_history.call_invocations, contains(
                '1stCall',
                '2ndCall',
                '3rdCall',
                '4thCall',
            ))

        def test_get_verified_indices(self):
            assert_that(self.call_invocation_history.verified_indices, contains())

        def test_get_verified_call_invocations(self):
            assert_that(self.call_invocation_history.verified_call_invocations, contains())

        def test_get_non_verified_indices(self):
            assert_that(self.call_invocation_history.non_verified_indices, contains(0, 1, 2, 3))

        def test_get_non_verified_call_invocations(self):
            assert_that(self.call_invocation_history.non_verified_call_invocations, contains(
                '1stCall',
                '2ndCall',
                '3rdCall',
                '4thCall',
            ))

    class TestWithVerifiedIndices(object):
        def setup_method(self, _):
            # noinspection PyAttributeOutsideInit
            self.call_invocation_history = mocki.core.NoMoreCallInvocationHistory(
                call_invocations=[
                    '1stCall',
                    '2ndCall',
                    '3rdCall',
                    '4thCall',
                ],

                verified_indices=[2, 3]
            )

        def test_get_call_invocations(self):
            assert_that(self.call_invocation_history.call_invocations, contains(
                '1stCall',
                '2ndCall',
                '3rdCall',
                '4thCall',
            ))

        def test_get_verified_indices(self):
            assert_that(self.call_invocation_history.verified_indices, contains(2, 3))

        def test_get_verified_call_invocations(self):
            assert_that(self.call_invocation_history.verified_call_invocations, contains(
                '3rdCall',
                '4thCall',
            ))

        def test_get_non_verified_indices(self):
            assert_that(self.call_invocation_history.non_verified_indices, contains(0, 1))

        def test_get_non_verified_call_invocations(self):
            assert_that(self.call_invocation_history.non_verified_call_invocations, contains(
                '1stCall',
                '2ndCall',
            ))

    def test_str(self):
        call_invocation_history = mocki.core.NoMoreCallInvocationHistory(
            call_invocations=[
                '1stCall',
                '2ndCall',
                '3rdCall',
                '4thCall',
            ],

            verified_indices=[2, 3]
        )

        assert_that(str(call_invocation_history), is_(equal_to('\n'.join([
            '  1stCall',
            '  2ndCall',
            'X 3rdCall',
            'X 4thCall',
        ]))))


# noinspection PyMethodMayBeStatic
class TestCallInvocation(object):
    def test_str_with_no_arg_or_kwarg(self):
        call_invocation = mocki.core.CallInvocation(
            'theMock', (), {}
        )

        assert_that(str(call_invocation), is_(equal_to(
            'theMock()'
        )))

    def test_str_with_args_but_no_kwarg(self):
        call_invocation = mocki.core.CallInvocation(
            'theMock', ('value1', 'value2'), {}
        )

        assert_that(str(call_invocation), is_(equal_to(
            "theMock('value1', 'value2')"
        )))

    def test_str_with_kwargs_but_no_arg(self):
        call_invocation = mocki.core.CallInvocation(
            'theMock', (), {'kw1': 'value1', 'kw2': 'value2'}
        )

        assert_that(str(call_invocation), is_(equal_to(
            "theMock(kw1='value1', kw2='value2')"
        )))

    def test_str_with_args_and_kwargs(self):
        call_invocation = mocki.core.CallInvocation(
            'theMock', ('value1', 'value2'), {'kw1': 'value1', 'kw2': 'value2'}
        )

        assert_that(str(call_invocation), is_(equal_to(
            "theMock('value1', 'value2', kw1='value1', kw2='value2')"
        )))
