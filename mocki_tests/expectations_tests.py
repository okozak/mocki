#
#   Copyright 2011 Olivier Kozak
#
#   This file is part of Mocki.
#
#   Mocki is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public
#   License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
#   version.
#
#   Mocki is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
#   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
#
#   You should have received a copy of the GNU Lesser General Public License along with Mocki. If not, see
#   <http://www.gnu.org/licenses/>.
#

import pytest

from hamcrest import *

import mocki.expectations


@pytest.mark.parametrize('nb_call_invocations, result', [
    (0, False),
    (1, True),
    (2, True),
])
def test_at_least_expectation(nb_call_invocations, result):
    expectation = mocki.expectations.AtLeast(1)

    call_invocations = ['theCall'] * nb_call_invocations

    assert_that(expectation(call_invocations), is_(result))


@pytest.mark.parametrize('nb_call_invocations, result', [
    (0, False),
    (1, True),
    (2, True),
])
def test_at_least_once_expectation(nb_call_invocations, result):
    expectation = mocki.expectations.AtLeastOnce()

    call_invocations = ['theCall'] * nb_call_invocations

    assert_that(expectation(call_invocations), is_(result))


@pytest.mark.parametrize('nb_call_invocations, result', [
    (0, True),
    (1, True),
    (2, False),
])
def test_at_most_expectation(nb_call_invocations, result):
    expectation = mocki.expectations.AtMost(1)

    call_invocations = ['theCall'] * nb_call_invocations

    assert_that(expectation(call_invocations), is_(result))


@pytest.mark.parametrize('nb_call_invocations, result', [
    (0, True),
    (1, True),
    (2, False),
])
def test_at_most_once_expectation(nb_call_invocations, result):
    expectation = mocki.expectations.AtMostOnce()

    call_invocations = ['theCall'] * nb_call_invocations

    assert_that(expectation(call_invocations), is_(result))


@pytest.mark.parametrize('nb_call_invocations, result', [
    (0, False),
    (1, True),
    (2, True),
    (3, True),
    (4, False),
])
def test_between_expectation(nb_call_invocations, result):
    expectation = mocki.expectations.Between(1, 3)

    call_invocations = ['theCall'] * nb_call_invocations

    assert_that(expectation(call_invocations), is_(result))


@pytest.mark.parametrize('nb_call_invocations, result', [
    (0, False),
    (1, True),
    (2, False),
])
def test_exactly_expectation(nb_call_invocations, result):
    expectation = mocki.expectations.Exactly(1)

    call_invocations = ['theCall'] * nb_call_invocations

    assert_that(expectation(call_invocations), is_(result))


@pytest.mark.parametrize('nb_call_invocations, result', [
    (0, True),
    (1, False),
])
def test_never_expectation(nb_call_invocations, result):
    expectation = mocki.expectations.Never()

    call_invocations = ['theCall'] * nb_call_invocations

    assert_that(expectation(call_invocations), is_(result))


@pytest.mark.parametrize('nb_call_invocations, result', [
    (0, False),
    (1, True),
    (2, False),
])
def test_once_expectation(nb_call_invocations, result):
    expectation = mocki.expectations.Once()

    call_invocations = ['theCall'] * nb_call_invocations

    assert_that(expectation(call_invocations), is_(result))
