#
#   Copyright 2011 Olivier Kozak
#
#   This file is part of Mocki.
#
#   Mocki is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public
#   License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
#   version.
#
#   Mocki is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
#   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
#
#   You should have received a copy of the GNU Lesser General Public License along with Mocki. If not, see
#   <http://www.gnu.org/licenses/>.
#

import pytest

from hamcrest import *

import mocki.matchers


def test_any_call_matcher():
    matcher = mocki.matchers.AnyCall()

    assert_that(matcher('theCall'), is_(True))


class TestCallMatcher(object):
    class Arg(object):
        def __init__(self, value):
            self.value = value

        def __eq__(self, other):
            return self.value == other.value

    @pytest.mark.parametrize('args, kwargs, result', [
        ((), {}, True),
        ((Arg('value1'), Arg('value2')), {}, False),
        ((), {'kw1': Arg('value1'), 'kw2': Arg('value2')}, False),
        ((Arg('value1'), Arg('value2')), {'kw1': Arg('value1'), 'kw2': Arg('value2')}, False),
    ])
    def test_with_no_arg_or_kwarg(self, args, kwargs, result):
        matcher = mocki.matchers.Call()

        call_invocation = ('theMock', args, kwargs)

        assert_that(matcher(call_invocation), is_(result))

    @pytest.mark.parametrize('args, kwargs, result', [
        ((), {}, False),
        ((Arg('value1'), Arg('value2')), {}, True),
        ((Arg('otherValue1'), Arg('otherValue2')), {}, False),
        ((), {'kw1': Arg('value1'), 'kw2': Arg('value2')}, False),
        ((Arg('value1'), Arg('value2')), {'kw1': Arg('value1'), 'kw2': Arg('value2')}, False),
    ])
    def test_with_args_but_no_kwarg(self, args, kwargs, result):
        matcher = mocki.matchers.Call(
            self.Arg('value1'), self.Arg('value2')
        )

        call_invocation = ('theMock', args, kwargs)

        assert_that(matcher(call_invocation), is_(result))

    @pytest.mark.parametrize('args, kwargs, result', [
        ((), {}, False),
        ((Arg('value1'), Arg('value2')), {}, True),
        ((Arg('otherValue1'), Arg('otherValue2')), {}, False),
        ((), {'kw1': Arg('value1'), 'kw2': Arg('value2')}, False),
        ((Arg('value1'), Arg('value2')), {'kw1': Arg('value1'), 'kw2': Arg('value2')}, False),
    ])
    def test_with_args_but_no_kwarg_using_some_callables(self, args, kwargs, result):
        class EqualTo(object):
            def __init__(self, expected_value):
                self.expected_value = expected_value

            def __call__(self, value):
                return value == self.expected_value

        matcher = mocki.matchers.Call(
            self.Arg('value1'), EqualTo(self.Arg('value2'))
        )

        call_invocation = ('theMock', args, kwargs)

        assert_that(matcher(call_invocation), is_(result))

    @pytest.mark.parametrize('args, kwargs, result', [
        ((), {}, False),
        ((Arg('value1'), Arg('value2')), {}, True),
        ((Arg('otherValue1'), Arg('otherValue2')), {}, False),
        ((), {'kw1': Arg('value1'), 'kw2': Arg('value2')}, False),
        ((Arg('value1'), Arg('value2')), {'kw1': Arg('value1'), 'kw2': Arg('value2')}, False),
    ])
    def test_with_args_but_no_kwarg_using_some_arg_matchers(self, args, kwargs, result):
        class EqualTo(object):
            def __init__(self, expected_value):
                self.expected_value = expected_value

            def matches(self, value):
                return value == self.expected_value

        matcher = mocki.matchers.Call(
            self.Arg('value1'), EqualTo(self.Arg('value2'))
        )

        call_invocation = ('theMock', args, kwargs)

        assert_that(matcher(call_invocation), is_(result))

    @pytest.mark.parametrize('args, kwargs, result', [
        ((), {}, False),
        ((Arg('value1'), Arg('value2')), {}, False),
        ((), {'kw1': Arg('value1'), 'kw2': Arg('value2')}, True),
        ((), {'kw1': Arg('otherValue1'), 'kw2': Arg('otherValue2')}, False),
        ((Arg('value1'), Arg('value2')), {'kw1': Arg('value1'), 'kw2': Arg('value2')}, False),
    ])
    def test_with_kwargs_but_no_arg(self, args, kwargs, result):
        matcher = mocki.matchers.Call(
            kw1=self.Arg('value1'), kw2=self.Arg('value2')
        )

        call_invocation = ('theMock', args, kwargs)

        assert_that(matcher(call_invocation), is_(result))

    @pytest.mark.parametrize('args, kwargs, result', [
        ((), {}, False),
        ((Arg('value1'), Arg('value2')), {}, False),
        ((), {'kw1': Arg('value1'), 'kw2': Arg('value2')}, True),
        ((), {'kw1': Arg('otherValue1'), 'kw2': Arg('otherValue2')}, False),
        ((Arg('value1'), Arg('value2')), {'kw1': Arg('value1'), 'kw2': Arg('value2')}, False),
    ])
    def test_with_kwargs_but_no_arg_using_some_callables(self, args, kwargs, result):
        class EqualTo(object):
            def __init__(self, expected_value):
                self.expected_value = expected_value

            def __call__(self, value):
                return value == self.expected_value

        matcher = mocki.matchers.Call(
            kw1=self.Arg('value1'), kw2=EqualTo(self.Arg('value2'))
        )

        call_invocation = ('theMock', args, kwargs)

        assert_that(matcher(call_invocation), is_(result))

    @pytest.mark.parametrize('args, kwargs, result', [
        ((), {}, False),
        ((Arg('value1'), Arg('value2')), {}, False),
        ((), {'kw1': Arg('value1'), 'kw2': Arg('value2')}, True),
        ((), {'kw1': Arg('otherValue1'), 'kw2': Arg('otherValue2')}, False),
        ((Arg('value1'), Arg('value2')), {'kw1': Arg('value1'), 'kw2': Arg('value2')}, False),
    ])
    def test_with_kwargs_but_no_arg_using_some_arg_matchers(self, args, kwargs, result):
        class EqualTo(object):
            def __init__(self, expected_value):
                self.expected_value = expected_value

            def matches(self, value):
                return value == self.expected_value

        matcher = mocki.matchers.Call(
            kw1=self.Arg('value1'), kw2=EqualTo(self.Arg('value2'))
        )

        call_invocation = ('theMock', args, kwargs)

        assert_that(matcher(call_invocation), is_(result))

    @pytest.mark.parametrize('args, kwargs, result', [
        ((), {}, False),
        ((Arg('value1'), Arg('value2')), {}, False),
        ((), {'kw1': Arg('value1'), 'kw2': Arg('value2')}, False),
        ((Arg('value1'), Arg('value2')), {'kw1': Arg('value1'), 'kw2': Arg('value2')}, True),
        ((Arg('otherValue1'), Arg('otherValue2')), {'kw1': Arg('value1'), 'kw2': Arg('value2')}, False),
        ((Arg('value1'), Arg('value2')), {'kw1': Arg('otherValue1'), 'kw2': Arg('otherValue2')}, False),
        ((Arg('otherValue1'), Arg('otherValue2')), {'kw1': Arg('otherValue1'), 'kw2': Arg('otherValue2')}, False),
    ])
    def test_with_args_and_kwargs(self, args, kwargs, result):
        matcher = mocki.matchers.Call(
            self.Arg('value1'), self.Arg('value2'), kw1=self.Arg('value1'), kw2=self.Arg('value2')
        )

        call_invocation = ('theMock', args, kwargs)

        assert_that(matcher(call_invocation), is_(result))

    @pytest.mark.parametrize('args, kwargs, result', [
        ((), {}, False),
        ((Arg('value1'), Arg('value2')), {}, False),
        ((), {'kw1': Arg('value1'), 'kw2': Arg('value2')}, False),
        ((Arg('value1'), Arg('value2')), {'kw1': Arg('value1'), 'kw2': Arg('value2')}, True),
        ((Arg('otherValue1'), Arg('otherValue2')), {'kw1': Arg('value1'), 'kw2': Arg('value2')}, False),
        ((Arg('value1'), Arg('value2')), {'kw1': Arg('otherValue1'), 'kw2': Arg('otherValue2')}, False),
        ((Arg('otherValue1'), Arg('otherValue2')), {'kw1': Arg('otherValue1'), 'kw2': Arg('otherValue2')}, False),
    ])
    def test_with_args_and_kwargs_using_some_callables(self, args, kwargs, result):
        class EqualTo(object):
            def __init__(self, expected_value):
                self.expected_value = expected_value

            def __call__(self, value):
                return value == self.expected_value

        matcher = mocki.matchers.Call(
            self.Arg('value1'), EqualTo(self.Arg('value2')), kw1=self.Arg('value1'), kw2=EqualTo(self.Arg('value2'))
        )

        call_invocation = ('theMock', args, kwargs)

        assert_that(matcher(call_invocation), is_(result))

    @pytest.mark.parametrize('args, kwargs, result', [
        ((), {}, False),
        ((Arg('value1'), Arg('value2')), {}, False),
        ((), {'kw1': Arg('value1'), 'kw2': Arg('value2')}, False),
        ((Arg('value1'), Arg('value2')), {'kw1': Arg('value1'), 'kw2': Arg('value2')}, True),
        ((Arg('otherValue1'), Arg('otherValue2')), {'kw1': Arg('value1'), 'kw2': Arg('value2')}, False),
        ((Arg('value1'), Arg('value2')), {'kw1': Arg('otherValue1'), 'kw2': Arg('otherValue2')}, False),
        ((Arg('otherValue1'), Arg('otherValue2')), {'kw1': Arg('otherValue1'), 'kw2': Arg('otherValue2')}, False),
    ])
    def test_with_args_and_kwargs_using_some_arg_matchers(self, args, kwargs, result):
        class EqualTo(object):
            def __init__(self, expected_value):
                self.expected_value = expected_value

            def matches(self, value):
                return value == self.expected_value

        matcher = mocki.matchers.Call(
            self.Arg('value1'), EqualTo(self.Arg('value2')), kw1=self.Arg('value1'), kw2=EqualTo(self.Arg('value2'))
        )

        call_invocation = ('theMock', args, kwargs)

        assert_that(matcher(call_invocation), is_(result))
