#
#   Copyright 2011 Olivier Kozak
#
#   This file is part of Mocki.
#
#   Mocki is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public
#   License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
#   version.
#
#   Mocki is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
#   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
#
#   You should have received a copy of the GNU Lesser General Public License along with Mocki. If not, see
#   <http://www.gnu.org/licenses/>.
#

from hamcrest import *

import mocki


# noinspection PyMethodMayBeStatic
class TestFake(object):
    def test_instantiation(self):
        fake = mocki.Fake(property='value', otherProperty='otherValue')

        # noinspection PyUnresolvedReferences
        assert_that(fake.property, is_(equal_to('value')))
        # noinspection PyUnresolvedReferences
        assert_that(fake.otherProperty, is_(equal_to('otherValue')))

    def test_equality(self):
        fake = mocki.Fake(property='value', otherProperty='otherValue')

        assert_that(fake.__eq__(mocki.Fake(property='value', otherProperty='otherValue')))
        assert_that(not fake.__eq__(mocki.Fake(property='otherValue', otherProperty='value')))

    def test_inequality(self):
        fake = mocki.Fake(property='value', otherProperty='otherValue')

        assert_that(fake.__ne__(mocki.Fake(property='otherValue', otherProperty='value')))
        assert_that(not fake.__ne__(mocki.Fake(property='value', otherProperty='otherValue')))

    def test_repr(self):
        fake = mocki.Fake(property='value', otherProperty='otherValue')

        assert_that(repr(fake), is_(equal_to("Fake(otherProperty='otherValue', property='value')")))


# noinspection PyMethodMayBeStatic
class TestMock(object):
    def test_str_with_named_mock(self):
        mock = mocki.Mock('theMock')

        assert_that(str(mock), is_(equal_to('theMock')))

    def test_str_with_unnamed_mock(self):
        mock = mocki.Mock()

        assert_that(str(mock), is_(equal_to('mock_{0}'.format(id(mock)))))


# noinspection PyMethodMayBeStatic
class TestMockObject(object):
    def test_str(self):
        mock = mocki.MockObject('theMock')

        assert_that(str(mock), is_(equal_to('theMock')))
