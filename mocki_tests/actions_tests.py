#
#   Copyright 2011 Olivier Kozak
#
#   This file is part of Mocki.
#
#   Mocki is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public
#   License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
#   version.
#
#   Mocki is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
#   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
#
#   You should have received a copy of the GNU Lesser General Public License along with Mocki. If not, see
#   <http://www.gnu.org/licenses/>.
#

import pytest

from hamcrest import *

import mocki.actions


def test_return_action():
    action = mocki.actions.Return('value')

    assert_that(action('theCall'), is_(equal_to('value')))


def test_raise_action():
    action = mocki.actions.Raise(Exception('error'))

    with pytest.raises(Exception) as excinfo:
        action('theCall')

    assert_that(str(excinfo.value), is_(equal_to('error')))


# noinspection PyMethodMayBeStatic
class TestInOrderAction(object):
    def test_with_one_action(self):
        action = mocki.actions.InOrder(
            lambda call_invocation: '1stAction-{0}'.format(call_invocation),
        )

        assert_that(action('1stCall'), is_(equal_to('1stAction-1stCall')))
        assert_that(action('2ndCall'), is_(equal_to('1stAction-2ndCall')))

    def test_with_several_actions(self):
        action = mocki.actions.InOrder(
            lambda call_invocation: '1stAction-{0}'.format(call_invocation),
            lambda call_invocation: '2ndAction-{0}'.format(call_invocation),
            lambda call_invocation: '3rdAction-{0}'.format(call_invocation),
        )

        assert_that(action('1stCall'), is_(equal_to('1stAction-1stCall')))
        assert_that(action('2ndCall'), is_(equal_to('2ndAction-2ndCall')))
        assert_that(action('3rdCall'), is_(equal_to('3rdAction-3rdCall')))
        assert_that(action('4thCall'), is_(equal_to('3rdAction-4thCall')))
