API reference
=============

.. autoclass:: mocki.core.Fake

.. autoclass:: mocki.core.Mock
    :members: on, verify, verify_no_more_call_invoked

.. autoclass:: mocki.matchers.AnyCall
.. autoclass:: mocki.matchers.Call

.. autoclass:: mocki.actions.Return
.. autoclass:: mocki.actions.Raise
.. autoclass:: mocki.actions.InOrder

.. autoclass:: mocki.expectations.AtLeast
.. autoclass:: mocki.expectations.AtLeastOnce
.. autoclass:: mocki.expectations.AtMost
.. autoclass:: mocki.expectations.AtMostOnce
.. autoclass:: mocki.expectations.Between
.. autoclass:: mocki.expectations.Exactly
.. autoclass:: mocki.expectations.Never
.. autoclass:: mocki.expectations.Once
